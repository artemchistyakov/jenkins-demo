#!/bin/bash
#Liveness Test
sleep 300
if (($(docker ps | grep -i test_instance | awk '{print $8}') == "Up")); then echo "test_instance OK"; else exit 1; fi
if (($(docker ps | grep -i redis | awk '{print $7}') == "Up")); then echo "redis OK"; else exit 1; fi